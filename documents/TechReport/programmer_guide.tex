
Animation programmers can write algorithms in notation that resembles
textbook pseudocode.
Java-like syntax is used throughout, e.g., node methods are called using
\texttt{v.m()}, where \texttt{v} is an object of type \texttt{Node} and \texttt{m} is a method.
The \texttt{graph} object is implicit -- so \texttt{graph.m()} is the same as \texttt{m()}.

Central to the Galant API is the \texttt{graph} object: currently all other
parts of the API refer to it.
The components of a graph are declared to be of type \texttt{Node} or
\texttt{Edge} and can be accessed/modified via a variety of
functions/methods.
When an observer or explorer interacts with the animation they move either
forward or backward one step at a time.
All aspects of the graph API therefore refer to the current \emph{state of
  the graph}, the set of states behaving as a stack.
API calls that change the state of a node or an edge automatically
generate a next step,
but the programmer can override this using a \texttt{beginStep()} and
\texttt{endStep()} pair. For example, the beginning of our implementation of
Dijkstra's algorithm looks like
\begin{verbatim}
beginStep();
for_nodes(node) {
    node.setWeight( INFINITY );
    nodePQ.add( node);
}
endStep();
\end{verbatim}
Without the override this initialization would require the observer to click
through multiple steps (one for each node) before getting to the interesting
part of the animation.

Functions and macros of the API are listed below.

\subsection{Graph methods}

Each node and edge has a unique integer id.
The id's are assigned consecutively as nodes/edges are created
and may not be altered.
The id of a node or edge can be accessed via the \texttt{getId()} method.

\begin{itemize}

\item
  \texttt{writeMessage(String s)}
  writes the string \texttt{s} at the top of the window (not really a graph method, but currently associated with the graph); if there is already a message present this method simply adds to it.
\item
  \texttt{getMessage()}
  returns the current message
\item
  \texttt{setDirected(boolean b)}
  causes the graph to be displayed and treated as directed (\texttt{b =
    true}) or undirected (\texttt{b=false}); this function can only be
  applied once at the beginning of the animation. It is useful, e.g., for
  minimum spanning tree algorithms (\texttt{b = false}) or for depth-first
  search to find strongly connected components (\texttt{b = true}).
\item
  \texttt{getNodes()}
  returns a list of nodes of the graph; the return type is the Java
templated \texttt{List<Node>}.\footnote{This will be replaced by a Galant-native
equivalent in a future version.}
\item
  \texttt{getEdges()}
  returns a list of edges of the graph; return type is \texttt{List<Edge>}
\item
  \verb+for_nodes( node )+ and \verb+for_edges( edge )+ circumvent the need to deal with Java templated list objects.
  Their syntax is:

    \verb+for_nodes( node ) {+ \emph{block of code that uses} \texttt{node} \verb+}+ (Note: \texttt{node} need not be declared.)

    and the corresponding for \verb+for_edges+.
\item
  \texttt{getNodeById(id)}
  returns the node with the given integer id or null if none exists; the only
  use in current animations is \texttt{getNodeById(0)} to specify node 0 (the first node
  created) as the start node for an algorithm that require one.
\item
  \texttt{getEdgeById(id)}
  returns the edge with the given integer id or null if none exists; not currently used.
\item
  \texttt{select(Node n)}
  makes node \texttt{n} the sole highlighted/selected node
\item
  \texttt{Node addNode()}
  returns a new node and adds it to the list of nodes; the id is one greater than the largest id so far;
attributes take on default values; weight, label, and position are absent and must be set explicitly
by appropriate method calls.
\item
  \texttt{addEdge(Node source, Node target)} adds an edge from the source to
  the target (source and target are immaterial when graph is undirected).
  There is also a variation with integer arguments that represent the id's of the nodes. As in the case of nodes, the edge is added to the list of edges and
its weight and label are absent.
\end{itemize}

\subsection{Node and edge methods}

Nodes and edges have `getters' and `setters' for
a variety of attributes, i.e.,
\texttt{set}$a$\texttt{([$a's$ type] x)} and [$a's$ type]
\texttt{get}$a$\texttt{)}.
In addition there are the usual methods one would expect from a graph API.

\subsubsection*{Logical attributes: functions and macros}

From a node's point of view we would like information about the adjacent nodes and incident edges.
The relevant \emph{methods} require the use of Java generics, but macros are provided
to simplify graph API access. The macros, which have equivalents in GDR, are:

\begin{itemize}

\item
\texttt{for\_adjacent(Node x, Edge e, Node y)\{ \emph{statements} \}}
executes the list of statements for each edge incident on \verb$x$.
The statements can refer to \verb$x$, or \verb$e$, the current incident edge,
or \verb$y$, the other endpoint of \verb$e$.

\item
\texttt{for\_outgoing(Node x, Edge e, Node y)\{ \emph{statements} \}}\\
behaves like \texttt{for\_adjacent} except that, when the graph is directed,
it iterates only over the edges whose source is \verb$x$ (it still iterates over all the edges when the graph is undirected). 

\item
\texttt{for\_incoming(Node x, Edge e, Node y)\{ \emph{statements} \}}\\
behaves like \texttt{for\_adjacent} except that, when the graph is directed,
it iterates only over the edges whose sink is \verb$x$ (it still iterates over all the edges when the graph is undirected). 

\end{itemize}

The actual API methods hiding behind these macros are (these are Node methods):

\begin{itemize}
\item
\texttt{List<Edge>~getIncidentEdges()} returns a list of all edges incident to this node,
both incoming and outgoing.
\item
\texttt{List<Edge>~getOutgoingEdges()} returns a list of edges directed away
from this node (all incident edges if the graph is undirected).
\item
\texttt{List<Edge>~getIncomingEdges()} returns a list of edges directed toward
this node (all incident edges if the graph is undirected).
\item
\texttt{Node~travel(Edge~e)} returns the other endpoint of \texttt{e}.
\end{itemize}

The logical edge methods are:

\begin{itemize}
\item
\texttt{setSourceNode(Node)} and \texttt{Node~getSourceNode()}
\item
\texttt{setDestNode(Node)} and \texttt{Node~getDestNode()}
\item
\texttt{getOtherEndPoint(Node~u)} returns \texttt{v} where this edge is
either \texttt{uv} or \texttt{vu}.
\end{itemize}

Nodes and edges also have a mechanism for setting (and getting)
arbitrary attributes of type Integer, String, and Double.
the relevant methods are
\texttt{setIntegerAttribute(String~key,Integer~value)} 
to associate an integer value with a node and
\texttt{Integer~getIntegerAttribute(String~key)} to retrieve it.
String and Double attributes work the same way as integer attributes.
These are useful when an algorithm requires arbitrary information to be
associated with nodes and/or edges.
Note that each individual node may have different attributes.

\subsubsection*{Geometric attributes}

Currently, the only geometric attributes are the positions of the
nodes. The edges are all straight lines and positions of labels are fixed.
The relevant methods for nodes are
\texttt{int~getX()}, \texttt{int~getY()}, and \texttt{Point~getPosition()}
for the 'getters'. To set a position,
one should use either \texttt{setPosition(Point)} or \texttt{setPosition(int,int)}.
However, once a node has an established position, it is possible to change
only one coordinate using \texttt{setX(int)} or \texttt{setY(int)}.

\subsubsection*{Display attributes}

Each node and edge has
both a (double) weight and a label.
The weight
is also a logical
attribute in that
it is used implicitly as a
key for
sorting and priority queues.
The label is simply text and may be interpreted however the programmer
chooses.
Both weight and label have values to indicate their absence:
\texttt{Double.NaN} (not a number) in the case of weights, \texttt{null}
in the case of labels.
In either case the weights/labels are not displayed.
Aside from the setters and getters: \texttt{setWeight(double)},
\mbox{\texttt{double getWeight()}}, \texttt{setLabel(String)},
and \mbox{\texttt{String getLabel()}}, the programmer can also
manipulate and test for the absence of weights/labels using
\texttt{clearWeight()} and \texttt{boolean~hasWeight()},
and the corresponding methods for labels.

Nodes can either be plain, highlighted (selected), marked (visited) or both highlighted and
marked.
Being highlighted alters the
the boundary (color and thickness) of a node (as controlled by the
implementation), while being marked affects the fill color.
Edges can be plain or selected, with thickness and color modified in the
latter case.

The relevant methods are:
\begin{itemize}
\item \texttt{highlight()}, \texttt{unHighlight()}, and \texttt{boolean~isHighlighted()}
\item correspondingly, \texttt{setSelected(true)}, \texttt{setSelected(false)},
and \texttt{boolean~isSelected()}
\item \texttt{mark()}, \texttt{unMark()}, and \texttt{boolean~isMarked()}
\item correspondingly, \texttt{setVisited(true)}, \texttt{setVisited(false)},
and \texttt{boolean~isVisited()}
\end{itemize}

Although the specific colors for displaying selected nodes or edges are
predetermined, the animation implementation can modify the color of a node boundary
or an edge, thus allowing for many different kinds of highlighting.\footnote{
It would also be desirable to be able to alter the thickness of edges or node boundaries to make color changes more visible.
}
The \texttt{setColor} and \texttt{getColor} methods use String arguments
using the RGB format \textsf{\#RRGGBB}; for example,
the string \texttt{\#0000ff} is blue.

Of the attributes listed above, weight, label, color and position can be
accessed and modified by the user as well as the program.
In all cases, modifications by execution of the animation are ephemeral --- the graph returns to its original state after execution.

\subsection{Additional programmer information}

A Galant algorithm/program is executed as a method within a Java class.
In order to shield the Galant programmer from Java ideosyncrasies,
some features have been added.

\subsubsection*{Definition of Functions/Methods}

A programmer can define arbitrary functions (methods) using the construct

\texttt{function} \textsl{[return\_type]} \textsl{name} \texttt{(}
 \textsl{parameters} \texttt{) \{} \\
 \hspace*{3em} \textsl{code\_block} \\
 \texttt{\}}

The behavior is like that of a Java method. So, for example,
\begin{verbatim}
function int plus( int a, int b ) {
    return a + b;
}
\end{verbatim}
is equivalent to
\begin{verbatim}
static int plus( int a, int b ) {
    return a + b;
}
\end{verbatim}

The \textsl{return\_type} is optional. If it is missing, the function behaves like
a \textsf{void} method in Java. An example is the recursive
\\
\texttt{function visit( Node v ) \{} \textsl{code} \texttt{\}}
\\
The conversion of functions into Java methods when Galant code is compiled
is complex and may result in indecipherable error messages.

\subsubsection*{Data Structures}

Galant provides some standard data strutures for nodes and edges automatically.
Each of these is supplied as a single instance.
\begin{itemize}

\item
  \texttt{nodeQ} -- a queue of elements of type \texttt{Node}; provides the
  the methods of a Java Queue:
  \begin{itemize}
    \item
      \texttt{offer(n)} -- puts node \verb+n+ on the queue
    \item
      \texttt{poll()} -- returns and removes the node at the front of the queue
    \item
      \texttt{peek()} -- returns the node at the front of the queue but does not remove it
    \item
      \texttt{isEmpty()} -- returns \textsf{true} if the queue is empty
  \end{itemize}

\item
  \texttt{edgeQ} -- a queue of elements of type \texttt{Edge}; methods are
  the same as those for \texttt{nodeQ}

\item
  \texttt{nodeStack} -- a stack of elements of type \texttt{Node}; provides
  methods of a Java Stack:\\
  \texttt{push(Node n)}, \texttt{pop()}, \texttt{peek()}, and \texttt{empty()}

\item
  \texttt{edgeStack} -- a stack of elements of type \texttt{Edge}; same as
  \texttt{nodeStack}

\item
  \texttt{nodePQ} -- a priority queue of elements of type \texttt{Node};
  provides standard priority queue methods:
  \begin{itemize}
    \item
      \texttt{offer(n)} -- puts node \verb+n+ on the priority queue; the priority is defined to be the weight of \verb+n+
    \item
      \texttt{poll()} -- returns and removes the node with the lowest priority
    \item
      \texttt{peek()} -- returns the node with the lowest priority but does not remove it
    \item
      \texttt{isEmpty()} -- returns \textsf{true} if the priority queue is empty
  \end{itemize}
  Unfortunately, the only way to implement the \textsf{decreaseKey} operation
  is to remove and reinsert the node.

  \item
    \texttt{edgePQ} -- analogous to \texttt{nodePQ}

\end{itemize}

Galant also provides classes corresponding to the above instances; these are
\begin{itemize}
  \item \texttt{NodeQueue}
  \item \texttt{EdgeQueue}
  \item \texttt{NodeStack}
  \item \texttt{EdgeStack}
  \item \texttt{NodePriorityQueue}
  \item \texttt{EdgePriorityQueue}
\end{itemize}

\subsubsection*{Global Variables}

One awkward feature of Galant's implementation is that global variables
must be declared \texttt{final}. For arrays and other structures this is not
a problem, except for the annoyance of the syntax. So, for example,
\begin{verbatim}
final int [] theArray = new int[ graph.getNodes.size() ];
\end{verbatim}
will work as expected: the entries of \verb+theArray+ can be modified as the animation progresses.
Not so with scalars. The workaround is along the lines of
\begin{verbatim}
class GlobalVariables {
    public int myInt;
    public double myDouble;
}
final GlobalVariables globals = new GlobalVariables();
\end{verbatim}
and then the globals need to be referred to as \verb+globals.myInt+
and \verb+globals.myDouble+, respectively.

% [Last modified: 2013 07 12 at 20:59:56 GMT]
